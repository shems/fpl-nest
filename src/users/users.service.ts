import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose'; 
import {Model} from 'mongoose';
import {User} from './user.model';
import {RegistrationDto} from './dto/registrationDto';
import {LoginDto} from './dto/loginDto';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class UsersService {
    constructor(@InjectModel('User')private readonly userModel: Model<User>){}

async register(registrationDto: RegistrationDto){
    var newUser= new this.userModel(registrationDto);
    return await newUser.save();
}
async findAll(){
    return await this.userModel.find();
}
async findUserById(id: String){
    return await this.userModel.findById(id);
}
async login(loginDto){

    console.log('login ', loginDto.username);
    
    const user= await this.userModel.findOne({"username": loginDto.username}).exec();

    if (!user)
        return { message: 'user not found' };

    if (!await user.comparePassword(loginDto.pwd, user.pwd))
        return { message: 'password does not match' }

    return this.createToken(user); 
}
async validateUser(loginDto){
    return await this.userModel.findOne({"userName" :loginDto.data.userName}).exec();
}
async createToken(user: User) {
    const expiresIn = 3600;
    user.pwd = null;
    return {
      message: 'OK',
      accessToken: jwt.sign({ data: user, exp: Math.floor(Date.now() / 1000) + (expiresIn * 1) }, 'secretKeyForTrelloProject'),
    };
  }

}
