import * as mongoose from 'mongoose';
import { Document} from 'mongoose';
import * as bcrypt from 'bcrypt';

export const UserSchema= new mongoose.Schema({
    username: {type: String, required: true},
    email: {type: String, required: true},
    pwd: {type: String, required: true},
    nbrVotes: {type: Number, required: false}
})

export interface User extends Document {
    id: string,
    username: string,
    email: string,
    pwd: string,
    nbrVotes: number,
    comparePassword(candidatePassword: string, userPass: string): any;
}

UserSchema.pre<User>('save',function (next) {
    const user = this;
    const salt = bcrypt.genSaltSync(10);
    user.pwd = bcrypt.hashSync(user.pwd,salt);
    next();
  });

UserSchema.methods.comparePassword = (candidatePassword: string, userPass: string) => {
    return bcrypt.compare(candidatePassword, userPass);
  };

  