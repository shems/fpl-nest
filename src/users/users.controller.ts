import { Controller, Get, Post, Patch, Delete, Body, Param, UseGuards, SetMetadata } from '@nestjs/common';
import { UsersService } from './users.service';
import { RegistrationDto } from './dto/RegistrationDto';
import { LoginDto } from './dto/loginDto';

@Controller('users')
export class UsersController {
    constructor(private uService: UsersService){}

  @Post('register')
    async register(@Body() registrationDto: RegistrationDto) {
      console.log('registration');
      return await this.uService.register(registrationDto);
  }
  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    return await this.uService.login(loginDto);
  }
  @Get('all')
    async all() {
      console.log('find All');
      return await this.uService.findAll();
  }
  
}