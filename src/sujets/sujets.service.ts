import { Injectable } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose'; 
import {Model} from 'mongoose';
import { SujetDto } from './dto/sujetDto';
import { Sujet } from './sujet.model';

@Injectable()
export class SujetsService {
    constructor(@InjectModel('Sujet') private readonly sujetModel: Model<Sujet>){}

    async insertSujet(sujetDto: SujetDto){
        const newSujet = new this.sujetModel(sujetDto);
        return await newSujet.save();
        
    }
    async getAllSujets(){
        console.log('get all sujets Service');
        
        return await this.sujetModel.find();
    }
    
    
    async findSujetById(id: String){
        return await this.sujetModel.findById(id)
    }

    async findSujetByIdAndVoteYes(id: String){
        return await this.sujetModel.updateOne({_id : id},{$push:{votes: "Oui"}});
    }

    async findSujetByIdAndVoteNo(id: String){
        return await this.sujetModel.updateOne({_id : id},{$push:{votes: "Non"}});
    }

    
}
