import { Module } from '@nestjs/common';
import { SujetsController } from './sujets.controller';
import { SujetsService } from './sujets.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SujetSchema } from './sujet.model';

@Module({
  imports:[MongooseModule.forFeature([{name:'Sujet', schema: SujetSchema}])],
  controllers: [SujetsController],
  providers: [SujetsService]
})
export class SujetsModule {}
