import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export const SujetSchema= new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    choix: {type:String, enum:['Oui', 'Nom'], required: false},
    votes:[{type: String,required: false}]
})

export interface Sujet extends Document{
        id: string; 
        title: string;
        choix: string;
        votes: string[];
}


