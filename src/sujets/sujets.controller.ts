import { Controller, Get, Post, Body, Param, Req } from '@nestjs/common';
import { SujetsService } from './sujets.service';
import { SujetDto } from './dto/sujetDto';

@Controller('sujets')
export class SujetsController {
    constructor(private sService: SujetsService){}
    
    @Get()
    async getAllSujets() {

      console.log('get all sujets Controller');
      const sujets = await this.sService.getAllSujets();
      return sujets;
    }
    @Post('add')
    async addsujet(@Body() sujetDto: SujetDto, @Req() req: any) {
      return await this.sService.insertSujet(sujetDto);
    }

    @Get('/:id')
    async getSujetById(@Param('id') id:string) {
      const sujet = await this.sService.findSujetById(id);
      return sujet
    }
    
    @Get('/:id/voteyes')
    async voteYesById(@Param('id') id:string) {
      const sujet = await this.sService.findSujetByIdAndVoteYes(id);
      return sujet
    }
    @Get('/:id/voteno')
    async voteNoById(@Param('id') id:string) {
      const sujet = await this.sService.findSujetByIdAndVoteNo(id);
      return sujet
    }
    
    
    
  
}
